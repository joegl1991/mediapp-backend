package com.mitocode.model.generic;

/**
 * 
 * @author Hector Garcia
 *
 */
public interface GenericModel {

	String getCodigo();

	String getDescripcion();

}
