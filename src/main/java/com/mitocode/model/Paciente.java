package com.mitocode.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.mitocode.model.generic.GenericModelImpl;

/**
 * 
 * @author Hector Garcia
 *
 */
@Entity
@Table(name = "paciente")
public class Paciente extends GenericModelImpl {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idPaciente;

	@Column(length = 70, nullable = false)
	private String nombres;

	@Column(length = 70, nullable = false)
	private String apellidos;

	@Column(length = 8, nullable = false)
	private String dni;

	@Column(length = 150)
	private String direccion;

	@Column(length = 9)
	private String telefono;

	public Paciente() {
		super();
	}

	public Paciente(int idPaciente, String nombres, String apellidos, String dni, String direccion, String telefono) {
		super();
		this.idPaciente = idPaciente;
		this.nombres = nombres;
		this.apellidos = apellidos;
		this.dni = dni;
		this.direccion = direccion;
		this.telefono = telefono;
	}

	public int getIdPaciente() {
		return idPaciente;
	}

	public void setIdPaciente(int idPaciente) {
		this.idPaciente = idPaciente;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

}
