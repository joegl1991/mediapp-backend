package com.mitocode.model.dto;

import java.util.List;

import com.mitocode.model.Consulta;
import com.mitocode.model.Examen;

/**
 * 
 * @author Hector Garcia
 *
 */
public class ConsultaListaExamenDTO {

	private Consulta consulta;

	private List<Examen> listaExamenes;

	public Consulta getConsulta() {
		return consulta;
	}

	public void setConsulta(Consulta consulta) {
		this.consulta = consulta;
	}

	public List<Examen> getListaExamenes() {
		return listaExamenes;
	}

	public void setListaExamenes(List<Examen> listaExamenes) {
		this.listaExamenes = listaExamenes;
	}

}
