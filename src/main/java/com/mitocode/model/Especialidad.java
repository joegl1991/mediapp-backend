package com.mitocode.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.mitocode.model.generic.GenericModelImpl;

/**
 * 
 * @author Hector Garcia
 *
 */
@Entity
@Table(name = "especialidad")
public class Especialidad extends GenericModelImpl {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idEspecialidad;

	@Column(name = "nombre", nullable = false, length = 50)
	private String nombre;

	public Especialidad() {
		super();
	}

	public Especialidad(int idEspecialidad, String nombre) {
		super();
		this.idEspecialidad = idEspecialidad;
		this.nombre = nombre;
	}

	public int getIdEspecialidad() {
		return idEspecialidad;
	}

	public void setIdEspecialidad(int idEspecialidad) {
		this.idEspecialidad = idEspecialidad;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

}