package com.mitocode.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;

import com.mitocode.model.constraint.ConsultaExamenPK;
import com.mitocode.model.generic.GenericModelImpl;

/**
 * 
 * @author Hector Garcia
 *
 */
@Entity
@IdClass(value = ConsultaExamenPK.class)
public class ConsultaExamen extends GenericModelImpl {

	@Id
	private Examen examen;

	@Id
	private Consulta consulta;

	public Examen getExamen() {
		return examen;
	}

	public void setExamen(Examen examen) {
		this.examen = examen;
	}

	public Consulta getConsulta() {
		return consulta;
	}

	public void setConsulta(Consulta consulta) {
		this.consulta = consulta;
	}

}
