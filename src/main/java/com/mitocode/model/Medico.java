package com.mitocode.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.mitocode.model.generic.GenericModelImpl;

/**
 * 
 * @author Hector Garcia
 *
 */
@Entity
@Table(name = "medico")
public class Medico extends GenericModelImpl {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idMedico;

	@Column(length = 70, nullable = false)
	private String nombres;

	@Column(length = 70, nullable = false)
	private String apellidos;

	@Column(length = 12, nullable = false)
	private String CMP;

	public Medico() {
		super();
	}

	public Medico(int idMedico, String nombres, String apellidos, String cMP) {
		super();
		this.idMedico = idMedico;
		this.nombres = nombres;
		this.apellidos = apellidos;
		CMP = cMP;
	}

	public int getIdMedico() {
		return idMedico;
	}

	public void setIdMedico(int idMedico) {
		this.idMedico = idMedico;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getCMP() {
		return CMP;
	}

	public void setCMP(String cMP) {
		CMP = cMP;
	}

}
