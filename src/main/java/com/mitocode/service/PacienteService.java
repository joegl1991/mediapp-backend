package com.mitocode.service;

import com.mitocode.model.Paciente;
import com.mitocode.service.generic.CrudService;

/**
 * 
 * @author Hector Garcia
 *
 */
public interface PacienteService extends CrudService<Paciente, Integer> {

}
