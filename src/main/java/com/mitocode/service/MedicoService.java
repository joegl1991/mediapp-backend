package com.mitocode.service;

import com.mitocode.model.Medico;
import com.mitocode.service.generic.CrudService;

/**
 * 
 * @author Hector Garcia
 *
 */
public interface MedicoService extends CrudService<Medico, Integer> {

}
