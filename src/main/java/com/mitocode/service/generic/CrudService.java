package com.mitocode.service.generic;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * 
 * @author Hector Garcia
 *
 * @param <T>
 * @param <PK>
 * 
 * @description Interfaz de servicio generica para operaciones CRUD
 */
public interface CrudService<T, PK> {

	T registrar(T entity);

	T modificar(T entity);

	void eliminar(PK id);

	List<T> listar();

	T listarId(PK id);
	
	Page<T> listarPorPagina(Pageable pageable);

}
