package com.mitocode.service.generic;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.mitocode.dao.generic.GenericJpaRepository;

/**
 * 
 * @author Hector Garcia
 *
 * @param <T>
 * @param <PK>
 * 
 * @description Servicio generico para operaciones CRUD
 */
public class CrudServiceImpl<T, PK extends Serializable> implements CrudService<T, PK> {

	@Autowired
	protected GenericJpaRepository<T, PK> dao;

	@Override
	public T registrar(T entity) {
		return dao.save(entity);
	}

	@Override
	public T modificar(T entity) {
		return dao.save(entity);
	}

	@Override
	public void eliminar(PK id) {
		dao.delete(id);
	}

	@Override
	public List<T> listar() {
		return dao.findAll();
	}

	@Override
	public T listarId(PK id) {
		return dao.findOne(id);
	}

	@Override
	public Page<T> listarPorPagina(Pageable pageable) {
		return dao.findAll(pageable);
	}

}
