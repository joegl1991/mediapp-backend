package com.mitocode.service.impl;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mitocode.dao.ConsultaExamenDAO;
import com.mitocode.model.Consulta;
import com.mitocode.model.dto.ConsultaListaExamenDTO;
import com.mitocode.service.ConsultaService;
import com.mitocode.service.generic.CrudServiceImpl;

/**
 * 
 * @author Hector Garcia
 *
 */
@Service
public class ConsultaServiceImpl extends CrudServiceImpl<Consulta, Integer> implements ConsultaService {

	@Autowired
	private ConsultaExamenDAO consultaExamenDAO;

	@Override
	public Consulta registrar(Consulta arg) {
		arg.getDetalleConsulta().forEach(x -> {
			x.setConsulta(arg);
		});
		return super.registrar(arg);
	}

	@Transactional
	@Override
	public Consulta registrarConsultaExamenes(ConsultaListaExamenDTO entity) {
		Consulta cons = new Consulta();
		try {
			entity.getConsulta().getDetalleConsulta().forEach(d -> d.setConsulta(entity.getConsulta()));
			cons = dao.save(entity.getConsulta());
			entity.getListaExamenes()
					.forEach(e -> consultaExamenDAO.registrar(entity.getConsulta().getIdConsulta(), e.getIdExamen()));
		} catch (Exception e) {
			throw e;
		}
		return cons;
	}

}
