package com.mitocode.service.impl;

import org.springframework.stereotype.Service;

import com.mitocode.model.Examen;
import com.mitocode.service.ExamenService;
import com.mitocode.service.generic.CrudServiceImpl;

/**
 * 
 * @author Hector Garcia
 *
 */
@Service
public class ExamenServiceImpl extends CrudServiceImpl<Examen, Integer> implements ExamenService {

}
