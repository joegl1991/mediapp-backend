package com.mitocode.service.impl;

import org.springframework.stereotype.Service;

import com.mitocode.model.Medico;
import com.mitocode.service.MedicoService;
import com.mitocode.service.generic.CrudServiceImpl;

/**
 * 
 * @author Hector Garcia
 *
 */
@Service
public class MedicoServiceImpl extends CrudServiceImpl<Medico, Integer> implements MedicoService {

}
