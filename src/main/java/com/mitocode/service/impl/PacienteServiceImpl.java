package com.mitocode.service.impl;

import org.springframework.stereotype.Service;

import com.mitocode.model.Paciente;
import com.mitocode.service.PacienteService;
import com.mitocode.service.generic.CrudServiceImpl;

/**
 * 
 * @author Hector Garcia
 *
 */
@Service
public class PacienteServiceImpl extends CrudServiceImpl<Paciente, Integer> implements PacienteService {

}
