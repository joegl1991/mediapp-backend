package com.mitocode.service.impl;

import org.springframework.stereotype.Service;

import com.mitocode.model.Especialidad;
import com.mitocode.service.EspecialidadService;
import com.mitocode.service.generic.CrudServiceImpl;

/**
 * 
 * @author Hector Garcia
 *
 */
@Service
public class EspecialidadServiceImpl extends CrudServiceImpl<Especialidad, Integer> implements EspecialidadService {

}
