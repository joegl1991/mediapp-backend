package com.mitocode.service.impl;

import org.springframework.stereotype.Service;

import com.mitocode.model.DetalleConsulta;
import com.mitocode.service.DetalleConsultaService;
import com.mitocode.service.generic.CrudServiceImpl;

/**
 * 
 * @author Hector Garcia
 *
 */
@Service
public class DetalleConsultaServiceImpl extends CrudServiceImpl<DetalleConsulta, Integer>
		implements DetalleConsultaService {

}
