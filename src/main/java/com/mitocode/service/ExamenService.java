package com.mitocode.service;

import com.mitocode.model.Examen;
import com.mitocode.service.generic.CrudService;

/**
 * 
 * @author Hector Garcia
 *
 */
public interface ExamenService extends CrudService<Examen, Integer> {

}
