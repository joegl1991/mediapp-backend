package com.mitocode.service;

import com.mitocode.model.Consulta;
import com.mitocode.model.dto.ConsultaListaExamenDTO;
import com.mitocode.service.generic.CrudService;

/**
 * 
 * @author Hector Garcia
 *
 */
public interface ConsultaService extends CrudService<Consulta, Integer> {

	public Consulta registrarConsultaExamenes(ConsultaListaExamenDTO entity);

}
