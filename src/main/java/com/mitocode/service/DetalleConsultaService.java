package com.mitocode.service;

import com.mitocode.model.DetalleConsulta;
import com.mitocode.service.generic.CrudService;

/**
 * 
 * @author Hector Garcia
 *
 */
public interface DetalleConsultaService extends CrudService<DetalleConsulta, Integer> {

}
