package com.mitocode.service;

import com.mitocode.model.Especialidad;
import com.mitocode.service.generic.CrudService;

/**
 * 
 * @author Hector Garcia
 *
 */
public interface EspecialidadService extends CrudService<Especialidad, Integer> {

}
