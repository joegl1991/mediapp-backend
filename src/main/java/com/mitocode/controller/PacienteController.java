package com.mitocode.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mitocode.controller.generic.CrudController;
import com.mitocode.model.Paciente;
import com.mitocode.service.generic.CrudService;

/**
 * 
 * @author Hector Garcia
 *
 */
@RestController
@RequestMapping("/paciente")
public class PacienteController extends CrudController<Paciente, Integer> {

	@Autowired
	public PacienteController(CrudService<Paciente, Integer> service) {
		super(service);
	}

}
