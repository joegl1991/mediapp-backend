package com.mitocode.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mitocode.controller.generic.CrudController;
import com.mitocode.model.Consulta;
import com.mitocode.model.dto.ConsultaListaExamenDTO;
import com.mitocode.service.ConsultaService;
import com.mitocode.service.generic.CrudService;

/**
 * 
 * @author Hector Garcia
 *
 */
@RestController
@RequestMapping("/consulta")
public class ConsultaController extends CrudController<Consulta, Integer> {

	@Autowired
	private ConsultaService consultaService;

	@Autowired
	public ConsultaController(CrudService<Consulta, Integer> service) {
		super(service);
	}

	@PostMapping(value = "/registrarconexamenes", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Consulta> registrar(@RequestBody ConsultaListaExamenDTO consulta) {
		Consulta c = new Consulta();
		try {
			c = consultaService.registrarConsultaExamenes(consulta);
		} catch (Exception e) {
			return new ResponseEntity<Consulta>(c, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Consulta>(c, HttpStatus.OK);
	}

}
