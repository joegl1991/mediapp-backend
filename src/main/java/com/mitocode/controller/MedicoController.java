package com.mitocode.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mitocode.controller.generic.CrudController;
import com.mitocode.model.Medico;
import com.mitocode.service.generic.CrudService;

/**
 * 
 * @author Hector Garcia
 *
 */
@RestController
@RequestMapping("/medico")
public class MedicoController extends CrudController<Medico, Integer> {

	@Autowired
	public MedicoController(CrudService<Medico, Integer> service) {
		super(service);
	}

}
