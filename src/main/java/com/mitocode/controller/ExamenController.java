package com.mitocode.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mitocode.controller.generic.CrudController;
import com.mitocode.model.Examen;
import com.mitocode.service.generic.CrudService;

/**
 * 
 * @author Hector Garcia
 *
 */
@RestController
@RequestMapping("/examen")
public class ExamenController extends CrudController<Examen, Integer> {

	@Autowired
	public ExamenController(CrudService<Examen, Integer> service) {
		super(service);
	}

}
