package com.mitocode.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mitocode.controller.generic.CrudController;
import com.mitocode.model.DetalleConsulta;
import com.mitocode.service.generic.CrudService;

/**
 * 
 * @author Hector Garcia
 *
 */
@RestController
@RequestMapping("/detalleconsulta")
public class DetalleConsultaController extends CrudController<DetalleConsulta, Integer> {

	@Autowired
	public DetalleConsultaController(CrudService<DetalleConsulta, Integer> service) {
		super(service);
	}

}
