package com.mitocode.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mitocode.controller.generic.CrudController;
import com.mitocode.model.Especialidad;
import com.mitocode.service.generic.CrudService;

/**
 * 
 * @author Hector Garcia
 *
 */
@RestController
@RequestMapping("/especialidad")
public class EspecialidadController extends CrudController<Especialidad, Integer> {

	@Autowired
	public EspecialidadController(CrudService<Especialidad, Integer> service) {
		super(service);
	}

}
