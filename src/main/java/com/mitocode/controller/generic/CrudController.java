package com.mitocode.controller.generic;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.mitocode.service.generic.CrudService;

/**
 * 
 * @author Hector Garcia
 *
 * @param <T>
 * @param <PK>
 * 
 * @description Controlador generico para operaciones CRUD
 */
public class CrudController<T, PK> {

	protected CrudService<T, PK> service;

	public CrudController(CrudService<T, PK> service) {
		this.service = service;
	}

	/**
	 * 
	 * @return List<T>
	 */
	@GetMapping(value = "/listar", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<T>> listar() {
		List<T> entities = new ArrayList<>();
		try {
			entities = service.listar();
		} catch (Exception e) {
			new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<List<T>>(entities, HttpStatus.OK);
	}
	
	/**
	 * 
	 * @return Page<T>
	 */
	@GetMapping(value = "/listarPagina", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Page<T>> listarPorPagina(Pageable pageable) {
		Page<T> entities = null;
		try {
			entities = service.listarPorPagina(pageable);
		} catch (Exception e) {
			new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Page<T>>(entities, HttpStatus.OK);
	}

	/**
	 * 
	 * @param id
	 * @return T
	 */
	@GetMapping(value = "/listar/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<T> listarPorId(@PathVariable PK id) {
		T entity = null;
		try {
			entity = service.listarId(id);
		} catch (Exception e) {
			new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<T>(entity, HttpStatus.OK);
	}

	/**
	 * 
	 * @param entity
	 * @return {@link Integer}
	 */
	@PostMapping(value = "/registrar", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<T> registrar(@RequestBody T entity) {
		T newEntity = null;
		try {
			newEntity = service.registrar(entity);
		} catch (Exception e) {
			new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<T>(newEntity, HttpStatus.OK);
	}

	/**
	 * 
	 * @param entity
	 * @return {@link Integer}
	 */
	@PutMapping(value = "/actualizar", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<T> actualizar(@RequestBody T entity) {
		T entityUpdated = null;
		try {
			entityUpdated = service.modificar(entity);
		} catch (Exception e) {
			new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<T>(entityUpdated, HttpStatus.OK);
	}

	/**
	 * 
	 * @param id
	 * @return {@link Integer}
	 */
	@DeleteMapping(value = "/eliminar/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Integer> eliminar(@PathVariable PK id) {
		int resultado = 0;
		try {
			service.eliminar(id);
			resultado = 1;
		} catch (Exception e) {
			new ResponseEntity<Integer>(resultado, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Integer>(resultado, HttpStatus.OK);
	}

}
