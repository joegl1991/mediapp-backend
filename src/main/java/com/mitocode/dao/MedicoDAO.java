package com.mitocode.dao;

import org.springframework.stereotype.Repository;

import com.mitocode.dao.generic.GenericJpaRepository;
import com.mitocode.model.Medico;

/**
 * 
 * @author Hector Garcia
 *
 */
@Repository
public interface MedicoDAO extends GenericJpaRepository<Medico, Integer> {

}
