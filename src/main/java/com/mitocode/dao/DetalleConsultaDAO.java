package com.mitocode.dao;

import org.springframework.stereotype.Repository;

import com.mitocode.dao.generic.GenericJpaRepository;
import com.mitocode.model.DetalleConsulta;

/**
 * 
 * @author Hector Garcia
 *
 */
@Repository
public interface DetalleConsultaDAO extends GenericJpaRepository<DetalleConsulta, Integer> {

}
