package com.mitocode.dao.generic;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

/**
 * 
 * @author Hector Garcia
 *
 * @param <T>
 * @param <PK>
 * 
 * @description Interfaz de repositorio generica para operaciones CRUD
 */
@NoRepositoryBean
public interface GenericJpaRepository<T, PK extends Serializable> extends JpaRepository<T, PK> {

}
