package com.mitocode.dao;

import org.springframework.stereotype.Repository;

import com.mitocode.dao.generic.GenericJpaRepository;
import com.mitocode.model.Examen;

/**
 * 
 * @author Hector Garcia
 *
 */
@Repository
public interface ExamenDAO extends GenericJpaRepository<Examen, Integer> {

}
