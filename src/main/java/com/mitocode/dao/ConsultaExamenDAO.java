package com.mitocode.dao;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.mitocode.dao.generic.GenericJpaRepository;
import com.mitocode.model.ConsultaExamen;

public interface ConsultaExamenDAO extends GenericJpaRepository<ConsultaExamen, Integer> {

	@Modifying
	@Query(value = "INSERT INTO consulta_examen(id_consulta, id_examen) VALUES (:idConsulta, :idExamen)", nativeQuery = true)
	int registrar(@Param("idConsulta") Integer idconsulta, @Param("idExamen") Integer idexamen);

}
