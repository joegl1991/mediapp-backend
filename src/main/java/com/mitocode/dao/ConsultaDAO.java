package com.mitocode.dao;

import org.springframework.stereotype.Repository;

import com.mitocode.dao.generic.GenericJpaRepository;
import com.mitocode.model.Consulta;

/**
 * 
 * @author Hector Garcia
 *
 */
@Repository
public interface ConsultaDAO extends GenericJpaRepository<Consulta, Integer> {

}
