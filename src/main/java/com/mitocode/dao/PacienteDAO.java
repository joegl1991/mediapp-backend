package com.mitocode.dao;

import org.springframework.stereotype.Repository;

import com.mitocode.dao.generic.GenericJpaRepository;
import com.mitocode.model.Paciente;

/**
 * 
 * @author Hector Garcia
 *
 */
@Repository
public interface PacienteDAO extends GenericJpaRepository<Paciente, Integer> {

}
