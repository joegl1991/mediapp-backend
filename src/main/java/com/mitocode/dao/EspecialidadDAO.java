package com.mitocode.dao;

import org.springframework.stereotype.Repository;

import com.mitocode.dao.generic.GenericJpaRepository;
import com.mitocode.model.Especialidad;

/**
 * 
 * @author Hector Garcia
 *
 */
@Repository
public interface EspecialidadDAO extends GenericJpaRepository<Especialidad, Integer> {

}
